<?php

use PHPUnit\Framework\TestCase;

require "../function.php";


class MainTest extends TestCase
{
	public function testInsufficientValuesInInput()
	{
		$this->expectException(InvalidArgumentException::class);
		twoOldestAges([1]);
	}

	public function testNonNumericValuesInInput()
	{
		$this->expectException(InvalidArgumentException::class);
		twoOldestAges(["ciao", 5, 87, 45, 8, 8]);
	}


	public function testBasicTests()
	{
		$results = twoOldestAges([1.5, 5.5, 87.3, 45.8, 8, 8]);
		$this->assertEquals($results[0], 45.8);
		$this->assertEquals($results[1], 87.3);

		$results = twoOldestAges([1, 5, 87, 45, 8, 8]);
		$this->assertEquals($results[0], 45);
		$this->assertEquals($results[1], 87);

		$results = twoOldestAges([6, 5, 83, 5, 3, 18]);
		$this->assertEquals($results[0], 18);
		$this->assertEquals($results[1], 83);
	}

}
