<?php

// Return the two oldest/oldest ages within the array of ages passed in.
function twoOldestAges(array $ages): array
{
	// Ensure preconditions are verified
	validateInput($ages);

	$second = pickUpOldestValue($ages);
	$first = pickUpOldestValue($ages);

	return [$first, $second];
}

function validateInput(array $ages)
{
	// At least 2 elements
	if (count($ages) < 2)
	{
		throw new InvalidArgumentException("Invalid input. Input array must include at least 2 items.");
	}

	// All elements are numeric
	array_map(function ($element)
	{
		if (!is_numeric($element))
		{
			throw new InvalidArgumentException("Invalid input [$element]. All input values must be numeric.");
		}
	}, $ages);
}

function pickUpOldestValue(array &$ages)
{
	$value = max($ages);
	$key = array_search($value, $ages, true);
	unset($ages[$key]);
	return $value;
}
