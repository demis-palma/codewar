<?php

function breakChocolate(int $n, int $m)
{
	if ($n < 1 || $m < 1)
	{
		return 0;
	}

	return ($n - 1) + ($m - 1) * $n;
}


echo breakChocolate(4, 5);