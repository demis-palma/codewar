<?php

const ASCII_TO_CARDINAL_OFFSET = 96;

function alphabet_position(string $s): string
{
	$s = strtolower($s);
	$s = preg_replace('/[^a-z]+/', "", $s);

	$result = [];

	// Can't use str_split() since it produce at least one element even in case of empty string and input "" gives "-97" in output
	/*
	foreach (str_split($s) as $character)
	{
		$result[] = ord($character) - ASCII_TO_CARDINAL_OFFSET;
	}
	*/

	// Note that $s[$i] (as well as str_split) iterates the bytes in the string, not the characters, therefore it's not good for multibyte languages
	for ($i = 0; $i < strlen($s); ++$i)
	{
		$result[] = ord($s[$i]) - ASCII_TO_CARDINAL_OFFSET;
	}

	return implode(" ", $result);
}
