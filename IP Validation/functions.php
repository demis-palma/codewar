<?php

function isValidIP(string $str): bool
{
    return (bool)filter_var($str, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
}
